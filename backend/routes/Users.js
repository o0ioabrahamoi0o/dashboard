const express   = require('express')
const api       = express.Router()
const cors      = require('cors')
const jwt       = require('jsonwebtoken')
const bcrypt    = require('bcrypt')
const ip        = require('ip')


const User  = require('../models/User')
//const { isPrimitive } = require('sequelize/types/lib/utils')
api.use(cors())

process.env.SECRET_KEY  = '#F43wF$2qfq2ñm2o32[[Ñ!2]ae'
const emailexpresion    = /\w+@\w+\.+[a-z]/;
const espacioexpresion  = /^\S+$/;
const lynpatron         = /[`~!@#$%^&*()_°¬|+\-=?;:'',.<>\{\}\[\]\\\/]/gi;

api.post('/register', (req, res) => {
    const today     = new Date()
    const ipadress = ip.address()
    const userData  = {
        userFirstName   : req.body.userFirstName,
        userLastName    : req.body.userLastName,
        userName        : req.body.userName,
        userNick        : req.body.userNick,
        userEmail       : req.body.userEmail,
        userPwd         : req.body.userPwd,
        userPwdR        : req.body.userPwdR,
        userQuestion    : req.body.userQuestion,
        userResponse    : req.body.userResponse,
        userTerms       : req.body.userTerms,
        userIp          : ipadress,
        createTime      : today
    }
    if (userData.userFirstName === ''  || userData.userLastName === ''  || userData.userName === '' || userData.userEmail === '' ||  userData.userNick === ''  || userData.userPwd === '' || userData.userPwdR === '' || userData.userResponse === '' ) {
        res.status(400).json({ message: 'Por favor complete todos los campos.' })
    } else if (lynpatron.test(userData.userFirstName) || lynpatron.test(userData.userLastName)) {
        res.status(400).json({ message: 'El nombre y apellido no se permite caracteres especiales.' })
    } else if (!espacioexpresion.test(userData.userNick) || lynpatron.test(userData.userNick)) {
        res.status(400).json({ message: 'El nick no se permite espacios y caracteres especiales.' })
    } else if (!espacioexpresion.test(userData.userName) || lynpatron.test(userData.userName)) {
        res.status(400).json({ message: 'El usuario no puede tener espacios y caracteres especiales.' })
    } else if (!emailexpresion.test(userData.userEmail)) {
            res.status(400).json({ message: 'El correo ingresado no es valido.' })
    } else if (!espacioexpresion.test(userData.userPwd)) {
        res.status(400).json({ message: 'Su contraseña no puede tener espacios' })
    } else if (userData.userName.length < 5 || userData.userName.length > 10) {
        res.status(400).json({ message: 'El usuario tiene que tener minimo 5 a 10 caracteres.' })
    } else if (userData.userPwd.length != userData.userPwdR.length) {
            res.status(400).json({ message: 'La contraseña ingresada no son iguales.' })
    } else if (userData.userPwd.length < 5 || userData.userPwd.length > 10) {
        res.status(400).json({ message: 'La cotraseña tiene que tener minimo 5 a 10 caracteres.' })
    } else if (lynpatron.test(userData.userResponse)) {
        res.status(400).json({ message: 'La respuesta no puede tener caracteres especiales.' })
    } else if (userData.userTerms === '') {
        res.status(400).json({ message: 'Tiene que aceptar los terminos y condiciones.' })
    } else {
        User.findOne({
            where: {
                userName: req.body.userName
            }
        })
        .then(user => {
            if (!user) {
                User.findOne({
                    where: {
                        userEmail: req.body.userEmail
                    }
                })
                .then(email => {
                    if (!email) {
                        bcrypt.hash(req.body.userPwd, 10, (err, hash) => {
                            userData.userPwd = hash
                            User.create(userData)
                            .then(user => {
                                res.status(200).json({ message: 'El usuario ' + user.userName + ' fue creada exitosamente.' })
                            })
                            .catch(err => {
                                res.status(400).json({ message: 'Problema en el sistema de registro. ' + err })
                            })
                        })  
                    } else {
                        res.status(400).json({ message: 'Este Email ingresado ya existe.' })
                    }
                })
                .catch(err => {
                    res.status(400).json({ message: 'Error al registrar el usuario.' })
                })
                
            } else {
                res.status(400).json({ message: 'Este usuario ingresado ya existe.' })
            }
        })
        .catch(err => {
            res.status(400).json({ message: 'Hay un problema en el sistema de registro.' })
        })
    }
})


api.post('/login', (req, res) =>{
    const userData  = {
        userName    : req.body.userName,
        userPwd     : req.body.userPwd
    }
        User.findOne({
            where: {
                userName: userData.userName
            }
        })
        .then(user => {
            if(bcrypt.compareSync(userData.userPwd, user.userPwd)) {
                let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                    expiresIn: 1440
                })
                res.send(token)
            } else {
                res.status(400).json({ message: 'La contraseña que usted ingreso es incorrecta.' })
            }
            
        })
        .catch(err => {
            res.status(400).json({ err }) 
        })
})

api.get('/ranking', (req, res) =>{
    User.findAll()
    .then(ranking => {
        res.send(ranking)
    })
})
module.exports = api