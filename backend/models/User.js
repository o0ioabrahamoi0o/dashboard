const Sequelize = require('sequelize')
const db        = require('../database/db')

module.exports = db.sequelize.define(
    'usermaster',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        userFirstName : {
            type: Sequelize.STRING
        },
        userLastName : {
            type: Sequelize.STRING
        },
        userName : {
            type: Sequelize.STRING
        },
        userNick : {
            type: Sequelize.STRING
        },
        userEmail : {
            type: Sequelize.STRING
        },
        userPwd : {
            type: Sequelize.STRING
        },
        userQuestion : {
            type: Sequelize.STRING
        },
        userResponse : {
            type: Sequelize.STRING
        },
        userTerms : {
            type: Sequelize.STRING
        },
        userIp : {
            type: Sequelize.STRING
        },
        createTime : {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW 
        }
    },
    {
        timestamps: false
    }
)
