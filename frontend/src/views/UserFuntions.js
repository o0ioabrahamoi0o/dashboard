import axios from 'axios'

export const register = newUser => {
    return axios
    .post('api/register', {
        userFirstName: newUser.userFirstName,
        userLastName: newUser.userLastName,
        userName: newUser.userName,
        userNick: newUser.userNick,
        userPwd: newUser.userPwd,
        userPwdR: newUser.userPwdR,
        userEmail: newUser.userEmail,
        userQuestion: newUser.userQuestion,
        userResponse: newUser.userResponse,
        userTerms: newUser.userTerms 
    })
    .catch(info => {
        var errors = JSON.parse(JSON.stringify(info.response.data.message))
        console.log(errors)
        document.getElementById('ErrorRegister').innerHTML = errors
    })
}

export const login = user => {
    return axios
    .post('api/login', {
        userName: user.userName,
        userPwd: user.userPwd
    })
    .then(res => {
        localStorage.setItem('usertoken', res.data)
        return res.data
    })
    .catch(info => {
        var errors = JSON.parse(JSON.stringify(info.response.data.message))
        console.log(errors)
        document.getElementById('ErrorLogin').innerHTML = errors
    })
}

export const getRanking = () => {
    return axios
    .get('/api/ranking', {})
    .then(res => {
        return JSON.stringify(res.data)
    })
    .catch(info => {
        var errors = JSON.parse(JSON.stringify(info))
        document.getElementById('ErrorRanking').innerHTML = errors
    })
}

 