import React, {Component}                              from 'react'
import jwt_decode                                       from 'jwt-decode'
import avatars from './images/avatar.jpeg'



//CSS
import './css/setting.css'


class Setting extends Component {

  constructor() {
    super()
    this.state = {
        userFirstName: '',
        userLastName: '',
        userName: '',
        userNick:'',
        avatarImg: avatars,
        isLoading: true
    }
  } 

  componentDidMount() {
    setTimeout(() => this.setState({isLoading: false}), 1200)
    const token = localStorage.usertoken
    if (token) {
        const decoded = jwt_decode(token)
        this.setState({
            userFirstName: decoded.userFirstName,
            userLastName: decoded.userLastName,
            userNick: decoded.userNick
        })
    }        
  }

  cambiarImagen = (e) => {
    var avatarImg = document.getElementById('userAvatar');
    this.setState({avatarImg: require('./images/'+avatarImg.value)})
  }
  render() {
    const {avatarImg} = this.state
    return (
    <div>
      <label className="setting-hello">Hola! ヽ(ヅ)ノ</label>
      <label className="setting-name">{ this.state.userFirstName + ' ' + this.state.userLastName + '(' + this.state.userNick + ')'}</label> 
        <img  className="avatar" src={(avatarImg)} alt="avatar" accept="image/*"/>
      <br/>

      

      <select className="input-select-avatar"
              name="userAvatar"
              id="userAvatar"
              value={this.state.userAvatar}
              onChange={this.cambiarImagen}>
        <option value="avatar.jpeg">Escoje un avatar</option>
        <option value="avatar.jpeg">Ninja</option>
        <option value="avatar2.jpeg">Cyber lobo</option>
        <option value="avatar3.jpeg">Samurai</option>
        <option value="avatar4.jpeg">Lobo azul</option>
        <option value="avatar5.jpeg">Chu pink</option>
        <option value="avatar6.jpeg">Spy pink</option>
        <option value="avatar7.jpeg">Chica gamer</option>
        <option value="avatar8.jpeg">Konishi</option>
      </select>
    </div>
    )
  }
}

export default Setting