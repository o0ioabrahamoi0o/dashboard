import React, { Component}                              from 'react'
import jwt_decode                                       from 'jwt-decode'
import { BrowserRouter as Router, Route, Redirect, Link, Switch}     from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faHome, faTrophy, faSortDown, faDownload} from '@fortawesome/free-solid-svg-icons'

//CSS
import './css/dashboard.css'

//MODULOS
import Home                                           from './Sub/home/Home'
import Ranking                                           from './Sub/ranking/Ranking'
import Download                                           from './Sub/download/Download'
import Setting                                           from './Sub/setting/Setting'

class Dashboard extends Component {
    
    constructor() {
        super()
        this.state = {
            userFirstName: '',
            userLastName: '',
            userName: '',
            userNick:'',
            isLoading: true
        }
    } 

    logOut(e) {
        e.preventDefault()
        localStorage.removeItem('usertoken')
        this.props.history.push('/')
    }

    componentDidMount() {
        setTimeout(() => this.setState({isLoading: false}), 1200)
        const token = localStorage.usertoken
        if (token) {
            const decoded = jwt_decode(token)
            this.setState({
                userFirstName: decoded.userFirstName,
                userNick: decoded.userNick
            })
        }        
    }
    
    render () {
        if(this.state.isLoading){
            return(
                <div className="contenedor-ripple">
                    <div className="lds-ripple">
                        <div></div>
                        <div></div>
                    </div>
                </div>
            )
        }

        const token = localStorage.usertoken
        if (!token) {
            return <Redirect to="/"/>
        }
        return (
            <div className="App">
                <Router>
                    <header>
                        <div className="contenedor-menu-horizontal">
                            <div className="menu-horizontal">
                                <img  src={require('./images/avatar.png')} alt="avatar"/>
                                <div className="flecha-submenu">
                                <FontAwesomeIcon icon={faSortDown}/>
                                </div>
                                <div className="sub-menu">
                                    <a  href=" " className="m-submenu">MENU</a>
                                    <Link className="s-submenu" to="/dashboard/setting">Configurar Perfil</Link>
                                    <a className="s-submenu" onClick={this.logOut.bind(this)} href=" ">Salir</a>
                                </div>
                            </div>
                        </div>
                    </header>

                    <div className="contenedor-menu">
                        <div className="Menu">
                            <img  src={require('./images/avatar.png')} alt="avatar"/>
                            <br/>
                            <label className="nickName">{this.state.userNick}</label> 
                            
                            <hr/>

                            <div className="Menu-models">
                                <Link className="modulo-home" to="/dashboard/home"> 
                                <FontAwesomeIcon icon={faHome}/>
                                    <h4>INICIO</h4>
                                </Link>

                                <Link className="modulo-ranking" to="/dashboard/ranking"> 
                                <FontAwesomeIcon icon={faTrophy}/>
                                    <h4>RANKING</h4>
                                </Link>
                            

                                <Link className="modulo-download" to="/dashboard/download"> 
                                <FontAwesomeIcon icon={faDownload}/>
                                    <h4>DESCARGA</h4>
                                </Link>
                            </div>
                        </div>
                    </div>

                    <div className="contenedor-models">
                        <Switch>
                            <Route  path="/dashboard/home" component={Home}/>
                            <Route  path="/dashboard/ranking" component={Ranking}/>
                            <Route  path="/dashboard/download" component={Download}/>
                            <Route  path="/dashboard/setting" component={Setting}/>
                        </Switch>
                    </div>
                </Router> 
            </div>
        )
    }
}

export default Dashboard